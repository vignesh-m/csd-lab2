import numpy as np
from matplotlib import pyplot as plt

filename = 'replacement_data'
ns = 4 * 1024

time_front = np.zeros(ns, dtype=np.float32)
time_back = np.zeros(ns, dtype=np.float32)

with open(filename, 'r') as f:
    for line in f.readlines():
        tp, a, s, t = line.split()
        if tp == '0':
            time_front[int(s)] = float(t)
        else:
            time_back[int(s)] = float(t)


def average_reduce(arr, k):
    # converts to len(arr)/k array by doing averages
    return np.array([x.mean() for x in np.split(arr, len(arr) // k)])


def moving_average(arr, n):
    cum = np.cumsum(arr)
    return (cum[n:] - cum[:-n]) / n


reduce_factor = 128
# time_front = average_reduce(time_front, reduce_factor)
# time_back = average_reduce(time_back, reduce_factor)
time_front = moving_average(time_front, reduce_factor)
time_back = moving_average(time_back, reduce_factor)
plt.plot(range(1, len(time_front)+1), time_front)
plt.plot(range(1, len(time_back)+1), time_back)
plt.legend(['f-to-b', 'b-to-f'])
plt.title('Access times per set for associativity = 80')
plt.xlabel('Set number')
plt.ylabel('Latency')

plt.show()
# print(set_avg)
