#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cstdint>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__("rdtsc"
                         : "=a"(lo), "=d"(hi));
    return ((uint64_t)hi << 32) | lo;
}

void swap (int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void randomize ( int arr[], int n )
{
    srand ( time(NULL) );
 
    for (int i = n-1; i > 0; i--)
    {
        int j = rand() % (i+1);
        swap(&arr[i], &arr[j]);
    }
}

int main()
{
    const int CACHE_SIZE = 3145728;          // S = 3 MB
    const int LINE_SIZE = 64;                // B = 64 B
    const int N_SETS = 4 * 1024;             // N = 4K, assuming 12-way
    const int SET_SIZE = N_SETS * LINE_SIZE; // W
    const int N_ACCESS = 10000;
    const int MAX_ASSOC = 100;

    long *arr = (long *)malloc(CACHE_SIZE + MAX_ASSOC * SET_SIZE);
    long base = (long)arr;

    /*
    for (int assoc = 1; assoc < MAX_ASSOC; assoc++)
    {
        for (int set = 0; set < N_SETS; set++)
        {
            // fill list of size assoc with (base+B*set) + n*W
            long *list = (long *)malloc(assoc * sizeof(long));
            for (int i = 0; i < assoc; i++)
            {
                list[i] = base + LINE_SIZE * set + i * SET_SIZE;
            }

            // repeat access elements
            uint64_t t = rdtsc();
            for (int i = 0; i < N_ACCESS; i++)
            {
                tester = (long *)list[i % assoc];
                int x = *tester;
            }
            t = rdtsc() - t;
            float ft = ((float)t) / N_ACCESS;
            // printf("%d %d %f\n", assoc, set, ft);
        }
    }
    */

    int assoc = 80;

    for (int set = N_SETS-1; set >=0; set--)
    {
        // fill list of size assoc with (base+B*set) + n*W
        int* list_order = (int*)malloc(assoc*sizeof(int));
        for(int j=0; j<assoc; j++){
            list_order[j] = j;
        }
        randomize(list_order,assoc);

        // fill list of size assoc with (base+B*set) + n*W
        long start = base + LINE_SIZE * set;
        long first = start + SET_SIZE * list_order[0];


        long *p = (long *)first;
        for (int i = 1; i < assoc; i++)
        {
            long next = (long)start + SET_SIZE * list_order[i];
            *p = next;
            p = (long *)next;
        }
        *p = first; // cycle
        free(list_order);

        // repeat access elements
        uint64_t t = rdtsc();
        p = (long *)start;
        for (int i = 0; i < N_ACCESS; i++)
        {
            // tester = (long *)list[i % assoc];
            // int x = *tester;
            p = (long *)*p;
            // printf("got %d\n", p);
        }
        t = rdtsc() - t;
        float ft = ((float)t) / N_ACCESS;
        printf("1 %d %d %f\n", assoc, set, ft);

    }

    return 0;
}
