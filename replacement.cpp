#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cstdint>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__("rdtsc"
                         : "=a"(lo), "=d"(hi));
    return ((uint64_t)hi << 32) | lo;
}

int main()
{
    const int CACHE_SIZE = 3145728;          // S = 3 MB
    const int LINE_SIZE = 64;                // B = 64 B
    const int N_SETS = 4 * 1024;             // N = 4K, assuming 12-way
    const int SET_SIZE = N_SETS * LINE_SIZE; // W
    const int N_ACCESS = 10000;

    long *arr = (long *)malloc(2 * CACHE_SIZE * sizeof(long));
    long base = (long)arr;
    volatile long *tester = 0;

    int assoc = 90;

    // front to back
    for (int set = 0; set < N_SETS; set++)
    {
        // fill list of size assoc with (base+B*set) + n*W
        long *list = (long *)malloc(assoc * sizeof(long));
        for (int i = 0; i < assoc; i++)
        {
            list[i] = base + LINE_SIZE * set + i * SET_SIZE;
        }

        // repeat access elements
        uint64_t t = rdtsc();
        for (int i = 0; i < N_ACCESS; i++)
        {
            tester = (long *)list[i % assoc];
            int x = *tester;
        }
        t = rdtsc() - t;
        float ft = ((float)t) / N_ACCESS;
        printf("0 %d %d %f\n", assoc, set, ft);
    }

    // back to front
    for (int set = N_SETS - 1; set >= 0; set--)
    {
        // fill list of size assoc with (base+B*set) + n*W
        long *list = (long *)malloc(assoc * sizeof(long));
        for (int i = 0; i < assoc; i++)
        {
            list[i] = base + LINE_SIZE * set + i * SET_SIZE;
        }

        // repeat access elements
        uint64_t t = rdtsc();
        for (int i = 0; i < N_ACCESS; i++)
        {
            tester = (long *)list[i % assoc];
            int x = *tester;
        }
        t = rdtsc() - t;
        float ft = ((float)t) / N_ACCESS;
        printf("1 %d %d %f\n", assoc, set, ft);
    }

    return 0;
}
