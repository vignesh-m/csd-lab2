all: assoc_find1 replace replace2

assoc_find1: assoc_find1.cpp
	g++ -o assoc_find1 assoc_find1.cpp -std=c++11

replace: replace.cpp
	g++ -o replace replace.cpp -std=c++11

replace2: replace2.cpp
	g++ -o replace2 replace2.cpp -std=c++11

clean:
	rm replace2 replace assoc_find1