import numpy as np
from matplotlib import pyplot as plt

filename = 'assoc_find_data'
na = 100
ns = 4 * 1024

times = np.zeros((na, ns), dtype=np.float32)

with open(filename, 'r') as f:
    for line in f.readlines():
        a, s, t = line.split()
        times[int(a)][int(s)] = float(t)

times_sorted = np.sort(times, axis=1)
times_sorted = times_sorted[:, :-100]  # remove top 100, bot 100
times_sorted = times_sorted[:, 100:]  # remove top 100, bot 100
set_avg = times_sorted.mean(axis=1)[1:]

plt.plot(range(1, na), set_avg)
plt.title('Average latency vs associatitvity')
plt.xlabel('Associativity')
plt.ylabel('Avg access latency')
plt.show()

def moving_average(arr, n):
    cum = np.cumsum(arr)
    return (cum[n:] - cum[:-n]) / n

reduce_factor = 128
assocs = [8, 24, 40]
for a in assocs:
	x = times[a]
	x = moving_average(x,reduce_factor)
	# print(len(x))
	plt.plot(range(1, len(x)+1), x)

plt.title('Access times per set for different associativity')
plt.xlabel('Set number')
plt.ylabel('Latency')
plt.legend(['assoc=' + str(a) for a in assocs])
plt.show()
