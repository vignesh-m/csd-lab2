import numpy as np
from matplotlib import pyplot as plt

filename = 'replace_data'
ns = 4 * 1024

time_front = np.zeros(ns, dtype=np.float32)
time_back = np.zeros(ns, dtype=np.float32)

with open(filename, 'r') as f:
    for line in f.readlines():
        tp, a, s, t = line.split()
        if tp == '0':
            time_front[int(s)] = float(t)
        else:
            time_back[int(s)] = float(t)


def average_reduce(arr, k):
    # converts to len(arr)/k array by doing averages
    return np.array([x.mean() for x in np.split(arr, len(arr) // k)])


def moving_average(arr, n):
    cum = np.cumsum(arr)
    return (cum[n:] - cum[:-n]) / n


reduce_factor = 128
# time_front = average_reduce(time_front, reduce_factor)
# time_back = average_reduce(time_back, reduce_factor)
time_front = moving_average(time_front, reduce_factor)
time_back = moving_average(time_back, reduce_factor)
plt.plot(range(0, len(time_front)), time_front)
plt.plot(range(0, len(time_back)), time_back)
plt.legend(['f-to-b', 'b-to-f'])

plt.show()
# print(set_avg)
